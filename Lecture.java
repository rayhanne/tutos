import java.util.Scanner;  //packet java qui contient la classe Scanner

public class Lecture {

	public static void main(String[] args) {
		
		// la fonction scanner 
		Scanner clavier = new Scanner(System.in);

        // Partie d�claration 		
		 int age;
		 String nom;
		 double taille;
		
		// Partie lecture de donn�e
		System.out.println("Qu'elle est votre nom:");
		nom = clavier.nextLine();
		 System.out.print("Quelle est votre age?: ");
		 age = clavier.nextInt();
		 System.out.print("Quelle est votre taille?: ");
		 taille = clavier.nextDouble();
		
		// Partie sortie des r�sultats  
		 System.out.println("Bonjour "+nom);
		 System.out.print("Vous avez "+age+" ans");
		 System.out.println(" et vous mesurez "+taille+" m�tres");
	}

}
